# Benchmarker

`@stylearcade/benchmarker` is yet another benchmarking tool - however this one has a slightly different focus:
- All tests are executed in **round robin** to prevent thermal throttling from skewing results
- **Outliers** from garbage collection and measurement error **are removed** from final results
- **Rich (ascii) charting** is included enabling analysis of performance distribution and performance over time

## Installation

```
npm install @stylearcade/benchmarker
```

## Usage

```javascript
const crypto = require('crypto')
const Benchmark = require('@stylearcade/benchmarker')

const input32 = crypto.randomBytes(32)
const input512 = crypto.randomBytes(512)

new Benchmark()
  .add({ algorithm: 'md5', bytes: 32 }, () => crypto.createHash('md5').update(input32).digest())
  .add({ algorithm: 'md5', bytes: 512 }, () => crypto.createHash('md5').update(input512).digest())
  .add({ algorithm: 'sha256', bytes: 32 }, () => crypto.createHash('sha256').update(input32).digest())
  .add({ algorithm: 'sha256', bytes: 512 }, () => crypto.createHash('sha256').update(input32).digest())
  .run()
  .log()

/*
  Samples: 7,649
  Duration (Ms): 1,001

  summary

  algorithm  bytes  lbound (p = 5)  ubound (p = 95)  boundedMean  boundedStdev    vsBest
  ---------  -----  --------------  ---------------  -----------  --------------  ------
  md5        32     529,101/sec     418,410/sec      508,056/sec  11,902,458/sec  1.017x
  md5        512    403,226/sec     333,333/sec      390,970/sec  10,555,539/sec  1.322x
  sha256     32     540,541/sec     423,729/sec      516,821/sec  11,165,404/sec  1x
  sha256     512    485,437/sec     386,100/sec      465,575/sec  10,489,220/sec  1.11x
*/
```
## API

### `new Benchmark([config])`
Creates a new Benchmark.

#### `config.minSamples`
How many samples to take of each test. Default `268` (which gives a 5% margin of error as per https://measuringu.com/sample-size-recommendations/)

#### `config.minTime`
How long in milliseconds to run the benchmark for. Default `1000`.

#### `config.lboundPc`
The minimum percentile to include in the sample. Default `5`.
This effectively wipes out the top `x%` of samples which often include measurement errors.

#### `config.uboundPc`
The maximim percentile to include in the sample. Default `95`.
This effectively wipes out the bottom `(100-x)%` of samples which often include garbage collection events.

#### `config.opsDecimals`
How many decimal places to show in ops/second measurements. Default `0`.

#### `config.batchSize`
How many test executions to conduct before taking a sample. Default 10.
This reduces the impact of measurement errors.

### `.add(metadata, func) => [this]`
Adds a test. Returns the Benchmark instance.

#### `metadata`
A string or an object that describes the test.

#### `func`
A synchronous or asynchronous test to execute.

### `.run() => { print, log, raw }`
Runs the test. Returns three functions that can be used to generate output in various forms.

### `.run().print([options]) => Promise<string>`
Returns a string containing test output that can be logged to console or displayed in a textarea.

#### `options.summary`
Displays benchmark results in tabular form, as per example above. Default `true`.

#### `options.chartWidth`
Controls how many ascii characters are used to display each chart horizontally. Default `25`.

#### `options.chartHeight`
Controls how many ascii lines are used to display each chart vertically. Default `10`.

#### `options.timeline`
For each test, charts a timeline of measurements allowing you to see how quickly and effectively the javascript interpreter is able to optimise the function. Default `false`.

```
timeline
 x: time = [0...1000ms]
 y: ops/sec (bounded)

  307797.54 ┤                ╭╮
  296644.87 ┤   ╭───╮╭───────╯╰────╮╭
  285492.20 ┤   │   ││             ╰╯
  274339.53 ┤  ╭╯   ╰╯
  263186.86 ┤  │
  252034.20 ┤  │
  240881.53 ┤  │
  229728.86 ┤ ╭╯
  218576.19 ┤ │
  207423.52 ┤ │
  196270.85 ┼─╯
```

#### `options.histogram`
For each test, charts a histogram showing how frequently each percentile is achieved. The more skewed to the left, the better. Default `false`.

```
histogram
 x: p = [5..95]
 y: count

    1292.00 ┼╭─╮
    1164.10 ┼╯ │
    1036.20 ┤  │
     908.30 ┤  ╰╮
     780.40 ┤   │
     652.50 ┤   │
     524.60 ┤   ╰─╮
     396.70 ┤     │
     268.80 ┤     ╰╮
     140.90 ┤      ╰───╮ ╭╮
      13.00 ┤          ╰─╯╰──────────
```

#### `options.waterfall`
For each test, charts the operations per second achieved at each percentile. The more area under the curve, the better. Default `false`.

```
waterfall
 x: p = [5..95]
 y: ops/sec

   17154.95 ┼───╮
   16755.67 ┤   ╰─────────────────╮
   16356.38 ┤                     ╰╮
   15957.10 ┤                      │
   15557.82 ┤                      ╰╮
   15158.54 ┤                       │
   14759.26 ┤                       │
   14359.97 ┤                       │
   13960.69 ┤                       │
   13561.41 ┤                       │
   13162.13 ┤                       ╰
```

#### `options.color`
Applies a color to the output via `colors` package. Default `undefined`.

#### `options.indent`
Left-pads the output by the specified number of characters. Default `0`.

### `.run().log([options]) => Promise<void>`
Same as above, but logs the output to console.

### `.run().raw() => Promise<object>`
Returns the raw internal results of the Benchmark, allowing you to create your own results formatter. For usage, see the source code of the built-in formatter `./src/pretty-print.js`.
