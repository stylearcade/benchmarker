/*
 * Splits an array into the desired number of slices (last slice will be longest)
 */
function createSlices (arr, numSlices) {
  const sliceLength = Math.floor(arr.length / numSlices)
  const slices = []

  for (let i = 0; i < numSlices; i++) {
    const from = i * sliceLength
    const to = i < numSlices - 1
      ? (i + 1) * sliceLength
      : undefined

    slices.push(arr.slice(from, to))
  }

  return slices
}

/*
 * Copies adjacent values to fill any gaps in the chart
 */
function fillGaps (arr) {
  let firstValueIndex = 0

  while (arr[firstValueIndex] === undefined && firstValueIndex < arr.length) firstValueIndex++

  if (firstValueIndex === arr.length) throw Error('arr has no values')

  for (let i = 0; i < firstValueIndex; i++) {
    arr[i] = arr[firstValueIndex]
  }

  for (let i = firstValueIndex + 1; i < arr.length; i++) {
    if (arr[i] === undefined) arr[i] = arr[i - 1]
  }

  return arr
}

/*
 * Splices two multiline text inputs, arranging them horizontally
 * This is used to show the charts next to each other in the terminal output
 */
function spliceCharts (lhs, rhs, minSpace = 0) {
  const lines1 = lhs.split('\n')
  const lines2 = rhs.split('\n')
  const outputLines = []
  const outputLength = Math.max(lines1.length, lines2.length)
  const lhsWidth = lines1.reduce((a, v) => Math.max(a, v.length), 0)
  const blank = new Array(lhsWidth + 1).join(' ')
  const separator = new Array(minSpace + 1).join(' ')

  for (let i = 0; i < outputLength; i++) {
    const left = ((lines1[i] || '') + blank).slice(0, lhsWidth)
    const right = lines2[i] || ''

    outputLines.push(left + separator + right)
  }

  return outputLines.join('\n')
}

module.exports = { createSlices, fillGaps, spliceCharts }
