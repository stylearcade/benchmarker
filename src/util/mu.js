const hrtime = require('browser-process-hrtime')

/*
 * Get a high resolution timestamp
 */
function now () {
  return hrtime()
}

/*
 * Return the number of elapsed microseconds since the give high resolution timestamp
 */
function elapsed (startHrTime) {
  const dur = hrtime(startHrTime)

  // convert to microseconds (mu)
  return dur[0] * 1000000 + dur[1] / 1000
}

/*
 * Converts an elapsed microseconds measurement into operations per second
 */
function toOpsSec (mu) {
  return 1 / mu * 1000000
}

/*
 * Converts an elapsed microseconds measurement into formatted operations per second
 */
function toOpsSecPretty (mu, decimals) {
  return `${(toOpsSec(mu)).toLocaleString('en-US', { maximumFractionDigits: decimals })}/sec`
}

module.exports = {
  now,
  elapsed,
  toOpsSec,
  toOpsSecPretty
}
