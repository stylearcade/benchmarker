const asciichart = require('asciichart')
const ss = require('simple-statistics')
const easyTable = require('easy-table')
const mu = require('./util/mu')
const { createSlices, fillGaps, spliceCharts } = require('./util/charts')

require('colors')

function prettyPrint (state, options = {}) {
  const { results, config } = state
  const { lboundPc, uboundPc, opsDecimals } = config
  const { samples, duration, tests } = results
  const {
    summary = true,
    chartWidth = 25,
    chartHeight = 10,
    timeline = false,
    histogram = false,
    waterfall = false,
    indent = 0,
    color
  } = options

  let output = `Samples: ${samples.toLocaleString()}\n` +
    `Duration (Ms): ${duration.toLocaleString()}\n`

  for (const test of tests) {
    const { metadata, stats } = test
    const { lbound, ubound, boundedMean, boundedStdev } = stats.mu

    const pretty = {
      ...metadata
    }

    pretty[`lbound (p = ${lboundPc})`] = mu.toOpsSecPretty(lbound, opsDecimals)
    pretty[`ubound (p = ${uboundPc})`] = mu.toOpsSecPretty(ubound, opsDecimals)
    pretty.boundedMean = mu.toOpsSecPretty(boundedMean, opsDecimals)
    pretty.boundedStdev = mu.toOpsSecPretty(boundedStdev, opsDecimals)
    pretty.vsBest = `${stats.vsBest.toLocaleString()}x`

    test.pretty = pretty
  }

  if (timeline || histogram || waterfall) {
    for (const { timings, sortedBoundSample, stats, pretty } of tests) {
      const { lbound, ubound } = stats.mu

      output += '\n' + easyTable.print([pretty]) + '\n'

      const charts = []

      if (timeline) {
        const chart = []

        for (const slice of createSlices(timings, chartWidth)) {
          const filtered = slice.filter(v => v >= lbound && v <= ubound)

          if (filtered.length) {
            chart.push(mu.toOpsSec(ss.mean(filtered)))
          } else {
            chart.push(undefined)
          }
        }

        charts.push([
          'timeline',
          ` x: time = [0...${duration}ms]`,
          ' y: ops/sec (bounded)',
          '',
          asciichart.plot(fillGaps(chart), { height: chartHeight })
        ].join('\n'))
      }

      if (histogram) {
        const sliceLength = (ubound - lbound) / chartWidth
        const chart = []

        for (let slice = 0; slice < chartWidth; slice++) {
          chart.push(0)
        }

        for (const timing of sortedBoundSample) {
          const slice = Math.min(Math.floor((timing - lbound) / sliceLength), chartWidth - 1)

          chart[slice] += 1
        }

        charts.push([
          'histogram',
          ` x: p = [${lboundPc}..${uboundPc}]`,
          ' y: count',
          '',
          asciichart.plot(chart, { height: chartHeight })
        ].join('\n'))
      }

      if (waterfall) {
        const chart = []

        for (const slice of createSlices(sortedBoundSample, chartWidth)) {
          chart.push(mu.toOpsSec(ss.mean(slice)))
        }

        charts.push([
          'waterfall',
          ` x: p = [${lboundPc}..${uboundPc}]`,
          ' y: ops/sec',
          '',
          asciichart.plot(chart, { height: chartHeight })
        ].join('\n'))
      }

      let allCharts = ''

      for (const chart of charts) {
        if (!allCharts) allCharts = chart
        else allCharts = spliceCharts(allCharts, chart, 5)
      }

      output += allCharts + '\n'
    }
  }

  if (summary) {
    const rows = []

    for (const { pretty } of tests) {
      rows.push(pretty)
    }

    output += [
      '\nsummary',
      '',
      easyTable.print(rows)
    ].join('\n')
  }

  if (indent > 0) {
    const pad = new Array(indent + 1).join(' ')

    output = output.split('\n').map(line => `${pad}${line}`).join('\n')
  }

  if (color !== undefined) {
    output = output[color]
  }

  return output
}

module.exports = prettyPrint
