const ss = require('simple-statistics')
const mu = require('./util/mu')

function analyze (state) {
  const { results, config } = state
  const { lboundPc, uboundPc } = config
  const { samples, tests } = results

  const lboundIndex = Math.floor(samples * lboundPc / 100)
  const uboundIndex = Math.floor(samples * uboundPc / 100)

  let best

  for (const test of tests) {
    const { timings } = test
    const sorted = [...timings].sort((a, b) => a - b)
    const sortedBoundSample = sorted.slice(lboundIndex, uboundIndex)
    const boundedMean = ss.mean(sortedBoundSample)
    const boundedStdev = ss.standardDeviation(sortedBoundSample)

    if (best === undefined || boundedMean < best) {
      best = boundedMean
    }

    const stats = {
      mu: {
        lbound: sorted[lboundIndex],
        ubound: sorted[uboundIndex],
        boundedMean,
        boundedStdev
      },
      opsSec: {}
    }

    for (const [stat, value] of Object.entries(stats.mu)) {
      stats.opsSec[stat] = mu.toOpsSec(value)
    }

    test.sortedBoundSample = sortedBoundSample
    test.stats = stats
    test.sorted = sorted
  }

  for (const { stats } of tests) {
    stats.vsBest = stats.mu.boundedMean / best
  }

  results.lboundIndex = lboundIndex
  results.uboundIndex = uboundIndex

  return state
}

module.exports = analyze
