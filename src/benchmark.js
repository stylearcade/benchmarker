const assert = require('assert')
const mu = require('./util/mu')
const run = require('./run')
const analyze = require('./analyze')
const prettyPrint = require('./pretty-print')

class Benchmark {
  constructor (config = {}) {
    const {
      minSamples = 268, // 5% margin of error as per https://measuringu.com/sample-size-recommendations/
      minTime = 1000,
      lboundPc = 5,
      uboundPc = 95,
      opsDecimals = 0,
      batchSize = 1
    } = config

    this.config = {
      minSamples,
      minTime,
      lboundPc,
      uboundPc,
      opsDecimals,
      batchSize
    }

    this.tests = {}
  }

  add (metadata, func) {
    if (typeof metadata === 'string') metadata = { name: metadata }

    const id = JSON.stringify(metadata)

    assert(!this.tests[id], `Test already exists for ${id}`)

    const result = func()
    const isAsync = result && typeof result.then === 'function'

    this.tests[id] = { metadata, func, isAsync, timings: [] }

    return this
  }

  run () {
    const state = run(Object.values(this.tests), this.config).then(analyze)

    const print = async (options) => prettyPrint(await state, options)

    const log = async (options) => {
      const output = prettyPrint(await state, options)

      console.log(output)
    }

    const raw = () => state

    return {
      print,
      log,
      raw
    }
  }
}

Benchmark.mu = mu

module.exports = Benchmark
