const mu = require('./util/mu')

async function run (tests, config) {
  const { minSamples, minTime, batchSize } = config
  const ts = Date.now()

  let samples = 0
  let duration = 0

  while (samples < minSamples || duration < minTime) {
    for (const { func, timings, isAsync } of tests) {
      let total = 0

      for (let i = 0; i < batchSize; i++) {
        if (isAsync) {
          const ts = mu.now()
          await func()
          total += mu.elapsed(ts)
        } else {
          const ts = mu.now()
          func()
          total += mu.elapsed(ts)
        }
      }

      timings.push(total / batchSize)
    }

    samples++
    duration = Date.now() - ts
  }

  const state = {
    results: { samples, duration, tests },
    config
  }

  return state
}

module.exports = run
