const crypto = require('crypto')
const Benchmark = require('..')

const input32 = crypto.randomBytes(32)
const input512 = crypto.randomBytes(512)

new Benchmark()
  .add({ algorithm: 'md5', bytes: 32 }, () => crypto.createHash('md5').update(input32).digest())
  .add({ algorithm: 'md5', bytes: 512 }, () => crypto.createHash('md5').update(input512).digest())
  .add({ algorithm: 'sha256', bytes: 32 }, () => crypto.createHash('sha256').update(input32).digest())
  .add({ algorithm: 'sha256', bytes: 512 }, () => crypto.createHash('sha256').update(input512).digest())
  .run()
  .log()
