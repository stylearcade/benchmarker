const Benchmark = require('..')
const jkmyers = require('./lib/jkmyers')
const Yamd5 = require('./lib/yamd5')
const wasm = require('hash-wasm')
const crypto = require('crypto')

const bench = new Benchmark()

for (const bytes of [10, 100, 1e3, 1e4, 1e5, 1e6]) {
  const text = crypto.randomBytes(bytes).toString('base64').slice(0, bytes)

  bench.add({ alg: 'jkmyers', bytes }, () => jkmyers(text))
  bench.add({ alg: 'yamd5.utf8', bytes }, () => Yamd5.hashStr(text))
  bench.add({ alg: 'yamd5.ascii', bytes }, () => Yamd5.hashAsciiStr(text))
  bench.add({ alg: 'wasm', bytes }, () => wasm.md5(text))
  bench.add({ alg: 'native', bytes }, () => crypto.createHash('md5').update(text).digest('hex'))
}

bench
  .run()
  .log({
    summary: true,
    histogram: true,
    waterfall: true
  })
